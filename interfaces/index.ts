import { ICountries } from '../packages/interfaces/';

export interface ICountriesItems {
  countries: ICountries[];
}

export interface IHomePageProps {
  countries: ICountriesItems;
}
