// @ts-nocheck
import styled from 'styled-components';
import { Avatar } from './Card';

export const StyledCardContainer = styled.div`
  position: relative;
  width: 100%;
  padding: 1rem;
  text-align: center;
  margin-bottom: ${props => props.theme.spacing()};
  border-radius: ${props => props.theme.borderRadius};
  box-shadow: 0 0 5px #ccc;
  background-color: #ffffff;
`;

export const StyledCount = styled.span`
  display: block;
  min-width: 50px;
  max-width: 70px;
  padding: .25rem;
  margin: 0 auto;
  color: #fff;
  font-size: 12px;
  text-align: center;
  border: 0 solid purple;
  border-radius: 100px;
  background: purple;
`;

export const StyledAvatar = styled.div`
  width: 90px;
  padding: 30px;
  margin: 0 auto;
  text-align: center;
  font-size: 20px;
  color: #fff;
  border-radius: 50%;
  background: ${(props) => props.bg};
`;

