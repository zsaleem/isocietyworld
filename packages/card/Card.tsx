import {
  StyledCardContainer,
  StyledCount,
  StyledAvatar,
} from './Card.styled';

import { ICard } from './interfaces';

export type Avatar = {
  bg: string;
}

const bgColors = ['#bdbdbe', '#ec6337', '#613db0', '#285b2a', '#376af0', '#ed8597'];

export const Card: React.FC<ICard> = ({ data }: ICard) => {
  const getRandomBGColor = () => bgColors[Math.floor(Math.random() * bgColors.length)];

  return (
    <StyledCardContainer>
      {/*// @ts-ignore*/}
      <StyledAvatar bg={getRandomBGColor()}>{data.alpha_2}</StyledAvatar>
      <h4>{data.name}</h4>
      <StyledCount>{data.count}</StyledCount>
    </StyledCardContainer>
  );
}

