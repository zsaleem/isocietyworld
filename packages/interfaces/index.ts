export interface ICountries {
  alpha_2: string;
  name: string;
  count: number;
}

