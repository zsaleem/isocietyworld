import Head from 'next/head';
//import { SidebarLayout } from '@is-world/sidebar';
//import styles from './PrimaryLayout.module.css';

export interface IPrimaryLayout {
  children?: React.ReactNode;
}

export const PrimaryLayout: React.FC<IPrimaryLayout> = ({ children }) => {
  return (
    <>
      <Head>
        <title>Primary Layout Example</title>
      </Head>
      <main>
        {children}
      </main>
    </>
  );
};

// export default PrimaryLayout;

