// @ts-nocheck
import styled from 'styled-components';

export const StyledDropdownContainer = styled.select`
  width: 100%;
  padding: .50rem;
  margin-bottom: ${props => props.theme.spacing(6)};
  font-size: 1rem;
  color: #333;
  border: 1px solid red;
`;
