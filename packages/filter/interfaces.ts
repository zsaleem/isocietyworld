export type Base = {
  value: string;
}

export type TFilterProps<TValue> = {
  sortItems: (value: TValue | string | number) => void;
}

export interface IFilterProps {
  sortItems: (value: any) => void;
}

