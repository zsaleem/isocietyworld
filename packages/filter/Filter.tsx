import React from 'react';
import { Dropdown } from '@is-world/Dropdown';
import { Base, TFilterProps, IFilterProps } from './interfaces';

export const Filter:  React.FC<IFilterProps> = ({ sortItems }: IFilterProps) => {
  return (
    <Dropdown
      onChange={sortItems}
      values={[{id: 1, title: "name"}, {id: 2, title: "count"}]} />
  );
};

