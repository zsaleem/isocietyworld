// @ts-nocheck
import styled from 'styled-components';

export const ListContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto auto auto;
  grid-gap: 15px;
  @media (max-width: ${props => props.theme.breakpoints.l}) {
  	grid-template-columns: auto auto auto;
	}
  @media (max-width: ${props => props.theme.breakpoints.m}) {
  	grid-template-columns: auto auto;
	} 
  @media (max-width: ${props => props.theme.breakpoints.s}) {
  	grid-template-columns: auto;
	}
`;
