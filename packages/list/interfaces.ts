import { ICountries } from '../interfaces/';

export interface IListProps {
  countries: ICountries[];
}

