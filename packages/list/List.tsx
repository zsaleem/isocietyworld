import { Card } from '@is-world/Card';
import { ListContainer } from './List.styled';
import { IListProps } from './interfaces';

export const List: React.FC<IListProps> = ({ countries }: IListProps) => {
  return (
    <ListContainer>
        {countries?.map((country, index) => (
          <Card
            key={`${country.alpha_2}${country.name}${country.count}`}
            data={country}
          />
        ))}
    </ListContainer>
  );
};

