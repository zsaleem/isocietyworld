export type Base = {
  id: number;
  title: string;
};

export type DropdownProps<TValue> = {
  values: TValue[];
  onChange: (value: TValue | string | number) => void;
};

