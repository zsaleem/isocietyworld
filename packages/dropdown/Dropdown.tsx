import { Base, DropdownProps } from './types';
import { StyledDropdownContainer } from './Dropdown.styled';

export const Dropdown = <TValue extends Base>({ values, onChange }: DropdownProps<TValue>) => {
  return (
    <StyledDropdownContainer onChange={(e) => onChange(e.target.value)}>
      {values.map((value) => (
        <option key={value.title} value={value.title}>
          {value.title}
        </option>
      ))}
    </StyledDropdownContainer>
  );
};

