import styled from 'styled-components';
 
export const StyledHeaderContainer = styled.div`
  width: 100%;
  height: 50px;
  margin-bottom: 20px;
  box-shadow: 0px 3px 6px #888;
  background-color: ${props => props.theme.components.header.primary.background}; 
`;

