import { StyledHeaderContainer } from './Header.styled';
import { IHeader } from './interface';

export const Header: React.FC<IHeader> = () => {
  return (
    <StyledHeaderContainer />
  );
};

export default Header;

