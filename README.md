<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#folder-structure">Folder Structure</a></li>
    <li><a href="#git-strategy">Git Strategy</a></li>
    <li><a href="#architecture">Architecture</a></li>
    <li><a href="#contributions">Contributions</a></li>
    <li><a href="#dev-links">Dev Links</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot](screenshots/screenshot.png)](http://is-world.s3-website-us-east-1.amazonaws.com/)
This project make a request to backend resource to retrieve list of countries and renders it on the home page. There is a possibility for the end users to render the list of countries, which are in the Card components, sorted by name or by count.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

Below are the core technologies I used for this project.

* [Next.js](https://nextjs.org/)
* [React](https://reactjs.org/)
* [Typescript](https://www.typescriptlang.org/)
* [Styled-Components](https://styled-components.com/)
* [AWS](https://aws.amazon.com/)
* [Gitlab](https://gitlab.com/)
* [eslint](https://eslint.org/)
* [prettier](https://prettier.io/)
* [husky](https://github.com/typicode/husky)


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

To run this project, please follow the steps below.

### Prerequisites

To install node 16.17.0, run the command below.

* node
  ```sh
  nvm install v16.17.0
  ```

To install yarn run the command below.
* yarn
  ```sh
  npm install --global yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git@gitlab.com:zsaleem/isocietyworld.git
   ```
2. Install NPM packages
   ```sh
   yarn
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

To run the project, below command will help.

  ```sh
  yarn dev
  ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- FOLDER STRUCTURE -->
## Folder Structure

  ```bash
  .
  ├── README.md
  ├── commitlint.config.js
  ├── interfaces
  │   └── index.ts
  ├── jest.config.js
  ├── next-env.d.ts
  ├── next.config.js
  ├── out
  ├── package.json
  ├── packages
  │   ├── card
  │   │   ├── Card.styled.ts
  │   │   ├── Card.tsx
  │   │   ├── index.ts
  │   │   ├── interfaces.ts
  │   │   └── package.json
  │   ├── dropdown
  │   │   ├── Dropdown.styled.ts
  │   │   ├── Dropdown.tsx
  │   │   ├── index.ts
  │   │   ├── package.json
  │   │   └── types.ts
  │   ├── filter
  │   │   ├── Filter.styled.ts
  │   │   ├── Filter.tsx
  │   │   ├── index.ts
  │   │   ├── interfaces.ts
  │   │   └── package.json
  │   ├── header
  │   │   ├── Header.styled.ts
  │   │   ├── Header.tsx
  │   │   ├── index.ts
  │   │   ├── interface.ts
  │   │   ├── next.config.js
  │   │   ├── package.json
  │   │   └── utils.ts
  │   ├── interfaces
  │   │   ├── index.ts
  │   │   └── package.json
  │   ├── layout
  │   │   ├── PrimaryLayout.tsx
  │   │   ├── index.ts
  │   │   └── package.json
  │   └── list
  │       ├── List.styled.ts
  │       ├── List.tsx
  │       ├── index.ts
  │       ├── interfaces.ts
  │       └── package.json
  ├── pages
  │   ├── _app.tsx
  │   ├── _document.tsx
  │   ├── api
  │   │   └── hello.ts
  │   ├── index.tsx
  │   └── page.d.ts
  ├── public
  │   ├── favicon.ico
  │   └── vercel.svg
  ├── screenshots
  │   └── screenshot.png
  ├── styles
  │   ├── Home.module.css
  │   ├── HomePage.styled.ts
  │   └── globals.css
  ├── theme
  │   ├── index.ts
  │   └── theme.ts
  ├── tsconfig.json
  ├── types
  │   └── index.ts
  └── yarn.lock
  ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GIT STRATEGY -->
## Git Strategy

The git strategy I used for this project is `gitflow` workflow. The complete workflow is depicted in below screenshot.

![Screenshot](screenshots/Git.png)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ARCHITECTURE -->
## Architecture

Below is an Architecture I followed for this project.

![Screenshot](screenshots/architecture.png)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTIONS -->
## Contributions

In order to add new packages/components in the form of new features simply add a new `package` folder inside `packages` folder and give it a name of the component younare working on. Add `index.ts`, `PackageName.tsx`, & `package.json` files in that folder. Below is an example.

Create package folder such as `navigation` inside `/packages/` folder.

  ```sh
  mkdir navigation
  ```

Then create 3 mandatory files in it like below.

  ```ssh
  touch index.ts
  touch Navigation.ts
  touch package.json
  ```

Then fill the `package.json` file with following contents.

  ```bash
  {
    "name": "@is-world/Navigation",
    "main": "index.ts",
    "version": "x.x.x"
  }
  ```

Finally run below command in the root folder of your project.

  ```ssh
  yarn
  ```

<!-- DEV LINKS -->
## Dev Links

* [Gitlab Project](https://gitlab.com/zsaleem/isocietyworld)
* [Commits](https://gitlab.com/zsaleem/isocietyworld/-/commits/master)
* [Merge Requests](https://gitlab.com/zsaleem/isocietyworld/-/merge_requests?scope=all&state=all)
* [CI/CD Pipelines](https://gitlab.com/zsaleem/isocietyworld/-/pipelines)

