import React from 'react';
import type { NextPage } from 'next';
import { Header } from '@is-world/Header';
import { Filter } from '@is-world/Filter';
import { List } from '@is-world/List';
import { IHomePageProps } from '../interfaces/';
import { ICountries } from '@is-world/interfaces/';
import { StyledContainer } from '../styles/HomePage.styled';

const Home: NextPage<IHomePageProps> = ({ countries }: IHomePageProps) => {
  const [result, updateResult] = React.useState<ICountries[]>([]);

  React.useEffect(() => {
    updateResult(countries.countries);
  }, [countries]);

  const sortItems = React.useCallback((e: string) => {
    let sortByRespectiveCriteria = result?.slice(0);

    sortByRespectiveCriteria.sort((a: any, b: any) => {
      return a[e] < b[e]
              ? -1
              : a[e] > b[e]
                ? 1
                : 0;
    });

    updateResult(sortByRespectiveCriteria);
  }, [result]);

  return (
    <>
      <Header />
      <StyledContainer>
        <Filter sortItems={sortItems} />
        <List countries={result} />
      </StyledContainer>
    </>
  );
};

export async function getStaticProps() {
  const response = await fetch(`${process.env.NEXT_PUBLIC_URL}countries`);
  const countries = await response.json();

  return {
    props: {
      countries,
    },
  }
}

export default Home;

