export type Base = {
  id: string;
  title: string;
};

export type DropdownProps<TValue> = {
  values: TValue[];
  onChange: (value: TValue) => void;
};

